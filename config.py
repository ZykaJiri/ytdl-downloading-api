import configparser
from os.path import exists

# Init

CONFIG_FILE_NAME = 'youtube-downloader.conf'
config_path = f'./{CONFIG_FILE_NAME}'
config = configparser.ConfigParser()

config['PATHS'] = {
    'audio-download-path': 'your-path-here',
    'video-download-path': 'your-path-here'
}

config['YT-DLP-SETTINGS'] = {
    'base-command': 'yt-dlp --embed-thumbnail --embed-metadata'
}

if not exists(config_path):
    with open(config_path, 'w+') as configfile:
        config.write(configfile)


def read_config():
    config = configparser.ConfigParser()
    config.read(config_path)
    return config
