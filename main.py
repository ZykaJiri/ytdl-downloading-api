import os

from fastapi import FastAPI
from models import YoutubeDownloadEntity, YoutubeDownloadTypesEntity
from config import read_config

app = FastAPI()
config = read_config()
audio_path = config['PATHS']['audio-download-path']
video_path = config['PATHS']['video-download-path']
base_command = config['YT-DLP-SETTINGS']['base-command']


@app.post("/youtube-download")
async def youtube_download(download_entity: YoutubeDownloadEntity):
    if download_entity.type == YoutubeDownloadTypesEntity.audio:
        os.system(f'{base_command} -P home:{audio_path} -x {download_entity.url}')
        return 200

    if download_entity.type == YoutubeDownloadTypesEntity.video:
        os.system(f'{base_command} -P home:{video_path} {download_entity.url}')
        return 200

    return 500
