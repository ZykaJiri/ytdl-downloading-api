# YT-DLP simple api

## Installation
1. Install poetry
2. Clone the repository
3. Do `poetry install`
4. Start a server using: `uvicorn main:app --host 127.0.0.1 --port 80`
5. [OPTIONAL] Add systemd service - example (It is recommended to run the service as a different user):
```
[Unit]
Description=YTDLP downloading API
After=network.target
Requires=network.target

[Service]
User=root
WorkingDirectory=/var/www/ytdl-downloading-api
ExecStart=/usr/local/bin/poetry run uvicorn main:app --host 127.0.0.1 --port 25570
Restart=on-failure

[Install]
WantedBy=multi-user.target
```

6. Add a nginx configuration(Certificates will need to be added if not defined in nginx.conf):
```
server {
        listen 443 ssl;
        listen [::]:443 ssl;
        server_name host.name.com;
        location / {
                proxy_pass http://127.0.0.1:25570;
        }
}
```
7. Browse the API docs at http://127.0.0.1:25570/docs

