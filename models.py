from enum import Enum

from pydantic import BaseModel


class YoutubeDownloadTypesEntity(str, Enum):
    video = 'video'
    audio = 'audio'


class YoutubeDownloadEntity(BaseModel):
    url: str
    type: YoutubeDownloadTypesEntity
